<!DOCTYPE html>
<html>
<head>
	<title>Ejemplo Arreglos</title>
	<meta charset="utf-8">
</head>
<body>
	<?php
	echo"<h1>Calificaciones</h1>";

	$calificacionesSemestrales = array("Leo"=>$calificaciones=array(100, 100, 90, 70, 71, 72),
									   "Caro"=>$calificaciones=array(70, 71, 72,70, 71, 72),
									   "Daniela"=>$calificaciones=array(100, 100, 100, 100, 100, 90),
									   "Angel"=>$calificaciones=array(100, 100, 90, 100, 100, 90),
									   "Teresa"=>$calificaciones=array(70, 71, 72, 100, 100, 90,),
									   "José"=>$calificaciones=array(100, 100, 100, 100, 100, 90),
									   "María"=>$calificaciones=array(100, 100, 100, 100, 100, 90),
									   "Luis"=>$calificaciones=array(100, 100, 90, 100, 100, 90),
									   "Ernesto"=>$calificaciones=array(70, 71, 72, 100, 100, 90,),
									   "Deysi"=>$calificaciones=array(100, 100, 100, 100, 100, 90));


	/* For mejorado en Java
	for(Alumno a: coleccionObjetos) */

	$PromedioGeneral=0;
	$PromedioPersonal=0;
	$PromedioMateria=0;
	

	function obtenerPromedio($vector,$cantidad){
		$promedio=0;

		foreach ($vector as $key => $value) {

			for ($i=0; $i < sizeof($value) ; $i++) {

				//echo "<br>".$key."-->".$value[$i];
				$promedio=$value[$i]+$promedio;
			}
		}

		$promedio=$promedio/$cantidad;
		return $promedio;
		
	}


	//Promedio personal
	function obtenerPromedioPersonal($vector,$cantidad){

		$promedioPersonal=0;
		foreach ($vector as $key => $value) {

			for ($i=0; $i < sizeof($value) ; $i++) {

				$promedioPersonal=$value[$i]+$promedioPersonal;
			}

			echo "<br>".$key."-->     ".$promedioPersonal/$cantidad;
			$promedioPersonal=0;

			
		}
		
	}

	//Promedio por materia
	function obtenerPromedioMateria($vector){

		$promedioMateria=0;

		for ($i=0; $i < 6; $i++) {	

			foreach ($vector as $key => $value) {

				$promedioMateria=$value[$i]+$promedioMateria;
		}

			
			echo "<br>"."Materia ".$i."-->   ".$promedioMateria/10;
			$promedioMateria=0;
		}

	}

	
	function obtenerPromedios($vector,$cantidad){

		$variableArreglo=0;
		$promedioPersonal=0;
		foreach ($vector as $key => $value) {

			for ($i=0; $i < sizeof($value) ; $i++) {

				$promedioPersonal=$value[$i]+$promedioPersonal;
			}
			
			$promedios[$variableArreglo]=$promedioPersonal/$cantidad;
			$promedioPersonal=0;
			$variableArreglo=$variableArreglo+1;
			
		}

		return $promedios;
		
	}

	function obtenerMejorPromedio($vector){

		$promedioMayor=0;
		for ($i=0; $i<(sizeof($vector)-1); $i++) {
	

			if($vector[$i]<$vector[$i+1]){

				$promedioMayor=$vector[$i+1];

			}else{
				$promedioMayor=$vector[$i];
			}
		}
		
		return $promedioMayor;

	}
	
	//Promedios por encima del general
	function obtenerCantidadPromedios($vector, $promedioGeneral){
		$promediosMayores=0;

		for ($i=0; $i<sizeof($vector); $i++) {
			

				if($vector[$i]>$promedioGeneral){

					$promediosMayores=$promediosMayores+1;

				}
		}

		return $promediosMayores;

	}

	function obtenerListadoAlumnos($vector){
		$variableArreglo=0;
		$promedioPersonal=0;
		$promedios=array(0);

		foreach ($vector as $key => $value) {

			echo "<br>".$key.": ";
			
			for ($i=0; $i < sizeof($value) ; $i++) {
				
				echo "".$value[$i].", ";
				$promedioPersonal=$value[$i]+$promedioPersonal;
			}

		
			echo "<br> Promedio -->    ".$promedioPersonal/6;
			echo "<br>";
			$promedios[$variableArreglo]=$promedioPersonal/6;

			$variableArreglo=$variableArreglo+1;
			$promedioPersonal=0;
		}


	}



	
	$promedioGeneral=obtenerPromedio($calificacionesSemestrales,60);
	echo"<br>Promedio General:".$promedioGeneral;
	echo"<br>";
	echo"<br> Promedio Personal:";
	echo"<br>";
	echo"<br>".obtenerPromedioPersonal($calificacionesSemestrales,6);
	echo "<br>";
	echo "<br>Promedio por materia: ";
	echo"<br>".obtenerPromedioMateria($calificacionesSemestrales);
	echo "<br>";
	$promedios=obtenerPromedios($calificacionesSemestrales,6);
	echo "Promedio mayor:".obtenerMejorPromedio($promedios); //Promedios
	echo "<br>";
	echo "No. Promedios Mayores al general: ".obtenerCantidadPromedios($promedios, $promedioGeneral);
	echo "<br>";
	echo "<br>===Listado de alumnos====";
	echo "<br>";
	echo obtenerListadoAlumnos($calificacionesSemestrales);


	

	


	?>

</body>
</html>